package HW2.task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    /**
     * метод оптимизирует хранение документов и позволяет производить поиск по id
     * @param documents список документов
     * @return карту документов
     */
    public static Map<Integer, Document> organizeDocuments(List<Document> documents){
        Map<Integer, Document> map = new HashMap<>();

        for (Document document : documents) {
            map.put(document.id, document);
        }

        return map;
    }

    public static void main(String[] args) {
        Document document1 = new Document(1, "one", 100);
        Document document2 = new Document(2, "two", 50);
        Document document3 = new Document(3, "three", 150);

        List<Document> list = new ArrayList<>();
        list.add(document1);
        list.add(document2);
        list.add(document3);


        System.out.println(organizeDocuments(list));
    }
}
