package HW2.task2;

import java.util.Arrays;


public class Main {

    /**
     * метод выявляет, является ли одна строка анаграммой другой
     *
     * @param s первая строка
     * @param t вторая строка
     * @return true или false
     */
    public static boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }

        char[] sToChar = s.toCharArray();
        Arrays.sort(sToChar);
        s = new String(sToChar);

        char[] tToChar = t.toCharArray();
        Arrays.sort(tToChar);
        t = new String(tToChar);

        return t.equals(s);

    }

    public static void main(String[] args) {
        System.out.println(isAnagram("мама", "лама"));
        System.out.println(isAnagram("банка", "кабан"));
        System.out.println(isAnagram("обезьянство", "светобоязнь"));
    }

}
