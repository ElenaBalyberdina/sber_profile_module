package HW2.additional;

import java.util.*;

public class Main {

    /**
     * метод для поиск наиболее встечающихся слов в тексте
     *
     * @param words массив слов (текст)
     * @param k     количество часто встречающихся слов
     * @return массив слов
     */
    public static String[] mostOccurringWords(String[] words, int k) {
        Map<String, Integer> wordsCountMap = new HashMap<>();

        for (String word : words) {
            wordsCountMap.put(word, wordsCountMap.getOrDefault(word, 0) + 1);
        }

        List<Map.Entry<String, Integer>> entryList = new ArrayList<>(wordsCountMap.entrySet());

        Comparator<Map.Entry<String, Integer>> comparatorByValue = Map.Entry.comparingByValue();

        Comparator<Map.Entry<String, Integer>> comparatorByKey = Map.Entry.comparingByKey();

        entryList.sort(comparatorByValue.thenComparing(comparatorByKey).reversed());

        String[] stringsResult;
        if (k <= entryList.size()) {
            stringsResult = new String[k];
            for (int i = 0; i < k; i++) {
                stringsResult[i] = entryList.get(i).getKey();
            }
        } else {
            stringsResult = new String[entryList.size()];
            for (int i = 0; i < entryList.size(); i++) {
                stringsResult[i] = entryList.get(i).getKey();
            }
        }
        return stringsResult;
    }

    public static void main(String[] args) {
        String[] text = new String[]{"Раз", "он", "в", "море", "закинул", "невод", ",",
                "Пришел", "невод", "с", "одною", "тиной", ".",
                "Он", "в", "другой", "раз", "закинул", "невод", ",",
                "Пришел", "невод", "с", "травой", "морскою", ".",
                "В", "третий", "раз", "закинул", "он", "невод", ",",
                "Пришел", "невод", "с", "одною", "рыбкой", "."};

        System.out.println(Arrays.toString(mostOccurringWords(text, 3)));
    }
}