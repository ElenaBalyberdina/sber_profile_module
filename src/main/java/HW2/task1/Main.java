package HW2.task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Main {


    /**
     * метод для поиска уникальных элементов в наборе
     *
     * @param list список элементов
     * @return список уникальных элементов
     */
    public static <T> Set<T> returnUnique(ArrayList<T> list) {
        return new HashSet<>(list);

    }

    public static void main(String[] args) {
        ArrayList<String> listOfStrings = new ArrayList<>();
        listOfStrings.add("мандарин");
        listOfStrings.add("яблоко");
        listOfStrings.add("яблоко");
        listOfStrings.add("гранат");
        System.out.println(returnUnique(listOfStrings));

        ArrayList<Integer> listOfInt = new ArrayList<>();
        listOfInt.add(1);
        listOfInt.add(4);
        listOfInt.add(1);
        listOfInt.add(1);
        listOfInt.add(4);

        System.out.println(returnUnique(listOfInt));
    }
}
