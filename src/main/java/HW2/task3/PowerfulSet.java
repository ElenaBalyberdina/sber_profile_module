package HW2.task3;

import java.util.HashSet;
import java.util.Set;


public class PowerfulSet {
    /**
     * метод для поиска пересечения двух наборов
     *
     * @param set1 первый набор
     * @param set2 второй набор
     * @return пересечение двух наборов
     */
    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> result = new HashSet<>(set1);
        result.retainAll(set2);
        return result;
    }

    /**
     * метод для поиска объединения двух наборов
     *
     * @param set1 первый набор
     * @param set2 второй набор
     * @return объединение двух наборов
     */
    public static <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> result = new HashSet<>(set1);

        result.addAll(set2);
        return result;
    }

    /**
     * метод для поиска элементов из первого набора, исключая элементы второго набора
     *
     * @param set1 первый набор
     * @param set2 второй набор
     * @return элементы из первого набора, исключая элементы второго набора
     */
    public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> result = new HashSet<>(set1);

        result.removeAll(set2);
        return result;
    }
}
