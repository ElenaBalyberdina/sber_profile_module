package HW6.dao;

import HW6.model.Book;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserDAO {
    private final Connection connection;

    public UserDAO(Connection connection) {
        this.connection = connection;
    }

    public void createTableUsers() throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "create table if not exists users(" +
                        "name varchar(50)," +
                        "surname varchar(50)," +
                        "birth_date timestamp," +
                        "phone_number varchar(15) primary key," +
                        "email varchar(50)," +
                        "books_title varchar(150));");

        preparedStatement.execute();
    }

    public void addUser(
            String name,
            String surname,
            String birthDate,
            String phoneNumber,
            String email,
            String booksTitle) throws SQLException, ParseException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "insert into users(name, surname, birth_date, phone_number, email, books_title)" +
                        "values (?,?,?,?,?,?);"
        );

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date dateSQLFormat = new Date(simpleDateFormat.parse(birthDate).getTime());

        preparedStatement.setString(1, name);
        preparedStatement.setString(2, surname);
        preparedStatement.setDate(3, dateSQLFormat);
        preparedStatement.setString(4, phoneNumber);
        preparedStatement.setString(5, email);
        preparedStatement.setString(6, booksTitle);
        preparedStatement.execute();
    }

    public List<Book> getBooksByPhone(String phoneNumber) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "select books_title from users where phone_number = ?;");

        preparedStatement.setString(1, phoneNumber);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<String> booksTitles = new ArrayList<>();

        while (resultSet.next()) {
            String stringOfBookTitles = resultSet.getString("books_title");
            booksTitles = Arrays.asList(stringOfBookTitles.split(","));
            booksTitles.replaceAll(String::trim);
        }

        BookDAO bookDAO = new BookDAO(connection);

        return bookDAO.getBooksByTitles(booksTitles);
    }

    public List<Book> getBooksByEmail(String email) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "select books_title from users where email = ?;");

        preparedStatement.setString(1, email);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<String> booksTitles = new ArrayList<>();

        while (resultSet.next()) {
            String stringOfBookTitles = resultSet.getString("books_title");
            booksTitles = Arrays.asList(stringOfBookTitles.split(","));
            booksTitles.replaceAll(String::trim);
        }

        BookDAO bookDAO = new BookDAO(connection);

        return bookDAO.getBooksByTitles(booksTitles);
    }
}
