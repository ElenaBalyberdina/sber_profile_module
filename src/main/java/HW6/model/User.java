package HW6.model;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User {
    private String name;
    private String surname;
    private Date birthDate;
    private String telephoneNumber;
    private String email;
    private String booksTitles;

}

