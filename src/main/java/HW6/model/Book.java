package HW6.model;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Book {

        private Long id;
        private String title;
        private String author;
        private Date dateAdded;
}

