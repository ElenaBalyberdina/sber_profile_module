package HW6;

import HW6.dao.BookDAO;
import HW6.dao.UserDAO;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements CommandLineRunner {

    private final BookDAO bookDAO;
    private final UserDAO userDAO;

    public Application(BookDAO bookDAO, UserDAO userDAO) {
        this.bookDAO = bookDAO;
        this.userDAO = userDAO;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        bookDAO.createTableBooks();
        userDAO.createTableUsers();

        userDAO.addUser(
                "Ivan",
                "Ivanov",
                "01.01.1988",
                "+79999999999",
                "ivanov@mail.ru",
                "Сказки, Басни");

        userDAO.addUser(
                "Petr",
                "Petrov",
                "02.02.1989",
                "+11110000000",
                "petrov@mail.ru",
                "Сказки, Кортик, Дикая собака динго");

        bookDAO.addBook("Сказки", "Владимир Сутеев");
        bookDAO.addBook("Кортик", "Анатолий Рыбаков");
        bookDAO.addBook("Дикая собака динго", "Рувим Фраерман");
        bookDAO.addBook("Басни", "Иван Крылов");

        System.out.println(userDAO.getBooksByPhone("+79999999999"));

        System.out.println(userDAO.getBooksByEmail("petrov@mail.ru"));


    }
}
