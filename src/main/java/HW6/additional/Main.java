package HW6.additional;

public class Main {
    public static boolean isArmstrongNumber(int number) {
        int y = 0;
        int x = 0;
        int temp = number;
        while (number > 0) {
            x = number % 10;
            number = number / 10;
            y = y + (x * x * x);
        }
        return temp == y;
    }

    public static boolean isSimpleNumber(int number) {
        if ((number > 2 && number % 2 == 0) || (number == 0) || (number == 1)) {
            return false;
        }
        for (int i = 3; i <= (int) Math.sqrt(number); i += 2) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(isArmstrongNumber(153));
        System.out.println(isSimpleNumber(13));
    }
}
