package HW6.config;

import HW6.dao.BookDAO;
import HW6.dao.UserDAO;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static HW6.DBConstants.*;

@Configuration
public class DBConfig {
    @Bean
    @Scope("singleton")
    public Connection connection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB_NAME,
                USER,
                PASSWORD
        );
    }

    @Bean
    @Scope("prototype")
    public BookDAO bookDAO() throws SQLException {
        return new BookDAO(connection());
    }

    @Bean
    @Scope("prototype")
    public UserDAO userDAO() throws SQLException {
        return new UserDAO(connection());
    }
}
