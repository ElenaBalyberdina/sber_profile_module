package HW3.task4;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        printAllInterfaces(ArrayList.class);
    }

    private static void printAllInterfaces(Class<?> aClass1) {
        while (aClass1 != null) {
            System.out.println("Class: " + aClass1.getName());
            Class<?>[] interfaces;
            interfaces = aClass1.getInterfaces();
            if (aClass1.getSuperclass() != null) {
                System.out.println("Interfaces: ");
                Arrays.stream(interfaces).forEach(aClass -> System.out.println(aClass.getName()));
                System.out.println();
            }
            aClass1 = aClass1.getSuperclass();
        }
        System.out.println();
    }
}
