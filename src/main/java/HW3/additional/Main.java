package HW3.additional;

import java.util.Deque;
import java.util.LinkedList;

public class Main {
    public static boolean isCorrectBracketSequence1(String string) {
        int count = 0;

        for (char c : string.toCharArray()) {
            if (c == '(') {
                count++;
            } else {
                if (count == 0) {
                    return false;
                }
                count--;
            }
        }
        return count == 0;
    }

    public static boolean isCorrectBracketSequence2(String string) {
        Deque<Character> charDeque = new LinkedList<>();
        for (char tmpChar : string.toCharArray()) {
            if (Character.isMirrored(tmpChar))
                if (Character.isMirrored(tmpChar + 2))
                    charDeque.addFirst(tmpChar);
                else charDeque.addLast(tmpChar);
        }
        if (charDeque.size() % 2 != 0) return false;
        for (int i = 0; i < charDeque.size() / 2; i++) {
            if ((charDeque.removeLast() - charDeque.removeFirst()) != 2) return false;
        }
        return true;
    }


    public static void main(String[] args) {

        System.out.println(isCorrectBracketSequence1("(()()())")); //true
        System.out.println(isCorrectBracketSequence1(")(")); //false
        System.out.println(isCorrectBracketSequence1("(()")); //false
        System.out.println(isCorrectBracketSequence1("((()))"));//true

        System.out.println(isCorrectBracketSequence2("{()[]()}"));//true
        System.out.println(isCorrectBracketSequence2("{)(}"));//false
        System.out.println(isCorrectBracketSequence2("[}"));//false
        System.out.println(isCorrectBracketSequence2("[{(){}}][()]{}"));//true

    }
}
