insert into flowers (name, price)
values ('Rose', 100);

insert into flowers (name, price)
values ('Lily', 50);

insert into flowers (name, price)
values ('Chamomile', 25);

insert into customers (name, phone_number)
values ('John', '+799999999999');

insert into customers (name, phone_number)
values ('Ella', '+19990000000');

insert into customers (name, phone_number)
values ('Anna', '+7888888888');

insert into orders (id_flower, id_customer, count_of_flowers, date_order)
values (1, 1, 11, now());

insert into orders (id_flower, id_customer, count_of_flowers, date_order)
values (1, 2, 250, now());

insert into orders (id_flower, id_customer, count_of_flowers, date_order)
values (2, 2, 101, now());

insert into orders (id_flower, id_customer, count_of_flowers, date_order)
values (2, 3, 450, now());

insert into orders (id_flower, id_customer, count_of_flowers, date_order)
values (3, 3, 350, now());

insert into orders (id_flower, id_customer, count_of_flowers, date_order)
values (3, 1, 55, now());
