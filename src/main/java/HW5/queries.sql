/* 1. По идентификатору заказа получить данные заказа и данные клиента,
создавшего этот заказ
*/
select orders.id_order,
       orders.date_order,
       flowers.name   as flower_name,
       orders.number_of_flowers,
       customers.name as customer_name,
       customers.phone_number,
       customers.agreement_to_use_pd
from orders
         left join customers on customers.id_customer = orders.id_customer
         left join flowers on flowers.id_flower = orders.id_flower
where customers.id_customer = 2;

/*2. Получить данные всех заказов одного клиента по идентификатору
клиента за последний месяц */
select *
from orders
where orders.id_customer = 1
  and extract(year from orders.date_order) = extract(year from now())
  and extract(month from orders.date_order) = extract(month from now());

/*3. Найти заказ с максимальным количеством купленных цветов, вывести их
название и количество
*/

select flowers.name             as flower_name,
       orders.number_of_flowers as max_number_of_flowers
from flowers
         inner join orders on flowers.id_flower = orders.id_flower
where orders.number_of_flowers = (select max(orders.number_of_flowers) from orders);

/*4. Вывести общую выручку (сумму золотых монет по всем заказам) за все
время
*/
select sum(orders.number_of_flowers * flowers.price) as total_revenue
from orders
         left join flowers on orders.id_flower = flowers.id_flower;

