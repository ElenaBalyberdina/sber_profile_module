package HW4;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class Main {

    /**
     * Метод для подсчета суммы четных чисел в промежутке от 1 до 100 включительно и вывода её на экран
     */
    public static void printSumOfEvenNumbers() {
        System.out.println(
                IntStream
                        .range(0, 101)
                        .filter(number -> number % 2 == 0)
                        .sum()
        );
    }

    /**
     * метод для подсчета реультата перемножения чисел в списке
     *
     * @param list список чисел
     * @return результат перемножения
     */
    public static int multiplying(List<Integer> list) {
        return list
                .stream()
                .reduce(1, (x, y) -> x * y);
    }

    /**
     * Метод для подсчета и вывода на экран количества непустых строк
     *
     * @param list список строк
     */
    public static void printCountNonEmptyStrings(List<String> list) {
        System.out.println(
                list
                        .stream()
                        .filter(str -> !str.equals(""))
                        .count()
        );
    }

    /**
     * Метод для сортировки списка вещественных чисел в обратном порядке (по убыванию)
     *
     * @param list список вещественных чисел
     * @return отсортированный по убыванию список
     */
    public static List<Double> sortingFloats(List<Double> list) {
        return list.stream().sorted(Comparator.reverseOrder()).toList();
    }

    /**
     * Метод для вывода списка строк в верхнем регистре
     *
     * @param list список строк
     */
    public static void printUpperCaseStrings(List<String> list) {
        System.out.println(list.stream().map(String::toUpperCase).toList());
    }

    /**
     * Метод трансформирует сет сето чисел в сет чисел
     *
     * @param setOfSets набор наборов чисел
     * @return набор чисел
     */
    public static Set<Integer> setTransformation(Set<Set<Integer>> setOfSets) {
        return setOfSets
                .stream()
                .flatMap(Collection::stream)
                .collect(Collectors
                        .toSet()
                );
    }

    public static void main(String[] args) {
        printSumOfEvenNumbers();
        System.out.println(multiplying(List.of(1, 2, 3, 4, 5)));
        printCountNonEmptyStrings(List.of("abc", "", "", "def", "qqq"));
        System.out.println(sortingFloats(List.of(5.5, 1.1, 2.2, 3.3)));
        printUpperCaseStrings(List.of("abc", "def", "qqq"));
        System.out.println(setTransformation(Set.of(Set.of(1, 2, 3), Set.of(4, 5, 6), Set.of(7, 8, 9))));

    }

}
