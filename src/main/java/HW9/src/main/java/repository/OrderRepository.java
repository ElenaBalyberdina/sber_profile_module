package HW9.src.main.java.repository;

import HW9.src.main.java.model.Film;
import HW9.src.main.java.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findByFilmIdIn(List<Film> films);
}