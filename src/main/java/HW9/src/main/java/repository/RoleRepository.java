package HW9.src.main.java.repository;

import HW9.src.main.java.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
