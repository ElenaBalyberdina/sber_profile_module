package HW1.task_1_and_2;

import java.util.Scanner;

public class Main {

    public static void generateUncheckedNPE(Integer nullInteger) {
        try {
            String s = nullInteger.toString();
        } catch (NullPointerException npe) {
            throw new MyUncheckedException("Возможно, объект не инициализирован", npe);
        }
    }

    public static void generateArrayIndexOutOfBoundsException() {
        int[] array = new int[10];
        try {
            array[10] = 10;
        } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
            throw new MyUncheckedException("Индекс находится за пределами размера массива", arrayIndexOutOfBoundsException);
        }
    }

    public static void generateCheckedException() throws MyCheckedException {
        System.out.println("Введите число от 0 до 100");
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        if ((num > 100) || (num < 0)) {
            throw new MyCheckedException(String.format("Вы ввели число %d, которое не находится в требуемом диапазоне: от 0 до 100", num));
        }
    }

    public static void main(String[] args) {
        generateUncheckedNPE(null);
        generateArrayIndexOutOfBoundsException();
        try {
            generateCheckedException();
        } catch (MyCheckedException e) {
            e.printStackTrace();
        }
    }


}
