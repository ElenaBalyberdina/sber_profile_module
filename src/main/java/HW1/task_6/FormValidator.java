package HW1.task_6;

import HW1.task_5.Gender;
import HW1.task_5.UpperCaseException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;

public class FormValidator {
    /**
     * Метод проверяет, что длина имени находится в пределах от 2 до 20 символов и первая буква заглавная
     *
     * @param str имя для валидации
     */
    public void checkName(String str) throws MyException, UpperCaseException {
        int length = str.length();
        char firstLetter = str.toCharArray()[0];
        if (length < 2 || length > 20) {
            throw new MyException(String.format("Длина имени %d не находится в допустимых пределах!", length));
        }

        if (Character.isLowerCase(firstLetter)) {
            throw new UpperCaseException("Первая буква должна быть звглавной!");
        }
    }


    /**
     * Метод проверяет, что дата рождения не
     * раньше 01.01.1900 и не позже текущей даты.
     *
     * @param str дата рождения для валидации
     */
    public void checkBirthdate(String str) throws MyException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.ENGLISH);
        LocalDate minDate = LocalDate.ofEpochDay(1900 - 1 - 1);
        LocalDate date = LocalDate.parse(str, formatter);
        LocalDate currentDate = LocalDate.now();
        if (date.isAfter(currentDate) || date.isBefore(minDate)) {
            throw new MyException("Введена некорректная дата!");
        }
    }

    /**
     * Метод проверяет, что пол корректно матчится в
     * enum Gender, хранящий Male и Female значения.
     *
     * @param str гендер для валидации
     */
    public void checkGender(String str) throws MyException {
        String normalized = str.toUpperCase();
        Arrays.stream(Gender.values())
                .filter(g -> g.name().startsWith(normalized))
                .findFirst()
                .orElseThrow(() -> new MyException(String.format("Нет такого гендера: %s", str)));
    }

    /**
     * Метод проверяет, что рост положительное
     * число и корректно конвертируется в double
     *
     * @param str рост для валидации
     */
    public void checkHeight(String str) throws MyException {
        double age = 0.00;
        try {
            age = Double.parseDouble(str);
        } catch (Exception e) {
            throw new MyException("Некорректный рост!", e);
        }
        if (age < 0) {
            throw new MyException("Рост не может быть отрицательным!");
        }
    }

}
