package HW1.additional._1;

import java.util.Arrays;
import java.util.Scanner;


public class Main {

    /**
     * Метод для поиска двух максимальных чисел в массивы с использованием Java Collections Framework.
     *
     * @param n     целое положительное число
     * @param array массив целых чисел
     */
    public static void twoMaxNumbers_1(int n, int[] array) {
        Arrays.sort(array);
        printTwoMax(n, array);
    }


    /**
     * Метод для поиска двух максимальных чисел в массивы с использованием Java Collections Framework и функционального подхода (стримы)
     *
     * @param n     целое положительное число
     * @param array массив целых чисел
     */
    public static void twoMaxNumbers_2(int n, int[] array) {
        array = Arrays.stream(array).sorted().toArray();
        printTwoMax(n, array);
    }

    /**
     * Метод для поиска двух максимальных чисел в массивы с использованием алгоритма сортировки пузырьком
     *
     * @param n     целое положительное число
     * @param array массив целых чисел
     */
    public static void twoMaxNumbers_3(int n, int[] array) {
        for (int i = 0; i < array.length - 1; ++i) {
            for (int j = 0; j < array.length - i - 1; ++j) {
                if (array[j + 1] < array[j]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
        printTwoMax(n, array);
    }

    private static void printTwoMax(int n, int[] array) {
        System.out.println(array[n - 1] + " " + array[n - 2]);
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        twoMaxNumbers_1(n, array);
        twoMaxNumbers_2(n, array);
        twoMaxNumbers_3(n, array);
    }

}
