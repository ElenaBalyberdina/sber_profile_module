package HW1.additional._2;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    /**
     * метод для поиска индекса элемента бинарным поиском
     * @param array массив для поиска
     * @param p число для поиска
     * @return индекс числа
     */
    public static int binarySearchIteratively(
            int[] array, int p) {
        int index = Integer.MAX_VALUE;
        int low = 0;
        int high = array.length - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (array[mid] < p) {
                low = mid + 1;
            } else if (array[mid] > p) {
                high = mid - 1;
            } else if (array[mid] == p) {
                index = mid;
                break;
            }
        }
        return index;
    }

    /**
     * метод для поиска индекса элемента бинарным поиском
     * @param array массив для поиска
     * @param p число для поиска
     * @return индекс элемента
     */
    public static int byBinarySearch(int[] array, int p) {
        return Arrays.binarySearch(array, p);
    }


    /**
     * Метод поиска индекса элемента простым перебором
     *
     * @param array массив для поиска
     * @param p     число для поиска
     * @return индекс
     */
    public static int bruteForceSearch(int[] array, int p) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == p) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        Arrays.sort(array);
        System.out.println(bruteForceSearch(array, p));
        System.out.println(byBinarySearch(array, p));
        System.out.println(binarySearchIteratively(array, p));
    }
}
