package HW1.task_4;

public class MyEvenException extends Exception {
    public MyEvenException() {
    }

    public MyEvenException(String message) {
        super(message);
    }

    public MyEvenException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyEvenException(Throwable cause) {
        super(cause);
    }
}
