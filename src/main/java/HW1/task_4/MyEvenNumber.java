package HW1.task_4;

public class MyEvenNumber {
    private int n;
    public MyEvenNumber(int n) throws MyEvenException {

        if (n % 2 != 0) {
            throw new MyEvenException(
                    String.format("Вы пытаетесь создать объект с нечетным числом %d", n));
        } else {
            this.n = n;
        }
    }
}
