package HW1.task_3;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class Main {
    /**
     * Метод, открывающий файл ./input.txt и сохраняющий в файл
     * ./output.txt текст из input, где каждый латинский строчный символ заменен на
     * соответствующий заглавный с использованием try with resources
     */
    public static void writeUpperCase() {
        try (FileReader reader = new FileReader("input.txt");
             FileWriter writer = new FileWriter("output.txt")) {
            while (reader.ready()) {
                writer.write(String.valueOf((char) reader.read()).toUpperCase());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        writeUpperCase();
    }
}

