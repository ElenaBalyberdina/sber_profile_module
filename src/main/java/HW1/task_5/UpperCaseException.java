package HW1.task_5;

public class UpperCaseException extends Exception {
    public UpperCaseException() {
    }

    public UpperCaseException(String message) {
        super(message);
    }

    public UpperCaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpperCaseException(Throwable cause) {
        super(cause);
    }
}
